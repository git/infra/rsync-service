# image is based on stage3-amd64
FROM gentoo/stage3-amd64:latest
COPY rsyncd.conf /opt/rsync/rsyncd.conf
COPY wrap_rsync.sh /opt/rsync/wrap_rsync.sh

# Normally I would advocate for ARG here and pass arguments to wrap_rsync.
# This would enable new docker builds with arguments like:
# docker build . --build_arg WAIT_TIME=30m -t gentoo/rsync
# However, ARG's cannot be passed to ENTRYPOINTs, so we set these as ENV instead.
# Mirror to get data from.
ENV SOURCE_MIRROR=rsync://turnstone.gentoo.org./gentoo-portage
# ENV SOURCE_MIRROR=rsync://rsync.us.gentoo.org/gentoo-portage
# Where to write the data in this container.
ENV DEST_DIR=/srv/gentoo
# How long to wait between syncs; must be a valid argument to sleep
ENV WAIT_TIME=30m

# This needs to exist in the container.
WORKDIR $DEST_DIR

# Expose Rsync port
EXPOSE 873

CMD mount -t tmpfs -o size=2g,nr_inodes=2000000 tmpfs /srv/gentoo

ENTRYPOINT /opt/rsync/wrap_rsync.sh
